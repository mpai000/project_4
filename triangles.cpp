
/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:Rudolph (Tutor)
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 7
 */

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include<cmath>
// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter() {
	return s1+s2+s3;
}

unsigned long triangle::area() {
	// TODO: write this function.
	// Note: why is it okay to return an integer here?  Recall that
	// all of our triangles have integer sides, and are right triangles...
	int a_t[] = {s1, s2, s3};
	sort(a_t, a_t + 3);
	return (1.0/2.0)*a_t[0]*a_t[1];
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2) {
	// TODO: write this function.
	int a_t1[] = {t1.s1, t1.s2, t1.s3};
	int a_t2[] = {t2.s1, t2.s2, t2.s3};
	sort(a_t1, a_t1 + 3);
	sort(a_t2, a_t2 + 3);
	bool temp = false;
	for(int i = 0; i<3; i++)
	{
		if(a_t1[i]==a_t2[i])
			temp = true;
		else
		{
			temp = false;
			break;
		}
	}
	return temp;
}

bool similar(triangle t1, triangle t2) {
	// TODO: write this function.
	int a_t1[] = {t1.s1, t1.s2, t1.s3};
	int a_t2[] = {t2.s1, t2.s2, t2.s3};
	sort(a_t1, a_t1 + 3);
	sort(a_t2, a_t2 + 3);
	bool temp = false;
	if(a_t1[0]%a_t2[0]==0 && a_t1[1]%a_t2[1]==0 && a_t1[2]%a_t2[2]==0 && a_t1[0]/a_t2[0]==a_t1[1]/a_t2[1] && a_t1[0]/a_t2[0]==a_t1[2]/a_t2[2])
		{
			temp = true;
		}
	else
	if(a_t2[0]%a_t1[0]==0 && a_t2[1]%a_t1[1]==0 && a_t2[2]%a_t1[2]==0 && a_t2[0]/a_t1[0]==a_t2[1]/a_t1[1] && a_t2[0]/a_t1[0]==a_t2[2]/a_t1[2])
		{
			temp = true;
		}
	return temp;
}
